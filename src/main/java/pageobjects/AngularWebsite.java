package pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AngularWebsite 
{
	WebDriver driver;
	
	@FindBy(xpath = "//a[text()='angular.io']")
	public WebElement angular_lnk;
	
	@FindBy(xpath = "//input[@type='search']")
	public WebElement search_txtbx;
	
	public AngularWebsite(WebDriver driver)
	{
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	public void angLinkClick()
	{
		angular_lnk.click();
	}
	
	public void setSearchText(String strsearch_txtbx)
	{
		search_txtbx.sendKeys(strsearch_txtbx);
	}
	
	public void searchText(String strsearch_txtbx)
	{
		this.angLinkClick();
		this.setSearchText(strsearch_txtbx);
	}
}
