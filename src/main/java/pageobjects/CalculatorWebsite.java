package pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class CalculatorWebsite 
{
	WebDriver driver;
	
	@FindBy(css="input[ng-model='first']")
	public WebElement first_txtBx;
	
	@FindBy(css="form input:nth-child(3)")
	public WebElement second_txtBx;
	
	@FindBy(css="button#gobutton.btn")
	public WebElement goBtn;
	
	//getResult
	@FindBy(how=How.CSS, using="tbody tr td:nth-child(3)")
	public WebElement getResult;
	
	public CalculatorWebsite(WebDriver driver)
	{
		this.driver = driver;
	    PageFactory.initElements(driver, this);
	}
	
	public void setFirstNumber(String strFirst_txtBx)
	{
		first_txtBx.sendKeys(strFirst_txtBx);
	}
	
	public void setSecondNumber(String strSecond_txtBx)
	{
		second_txtBx.sendKeys(strSecond_txtBx);
	}
	
	public void clickOnGO()
	{
		goBtn.click();
	}
	
	public void calcValue(String strFirst_txtBx, String strSecond_txtBx)
	{
		this.setFirstNumber(strFirst_txtBx);
		this.setSecondNumber(strSecond_txtBx);
		this.clickOnGO();
	}
	
}
