package reportgeneration;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

public class ExtentBaseTest 
{
	public static ExtentHtmlReporter htmlReporter;
    public static ExtentReports extent;
    public static ExtentTest test;
    public static WebDriver driver;
    public static final String UI_PROP = "ui";
    public static final String HTML_FILE = System.getProperty("user.dir")+"\\test-output\\myExtentReport.html";
    public static final boolean UI_FLAG = new Boolean(System.getProperty(UI_PROP)).booleanValue();
    
    @BeforeSuite
    public void setUpExtentReport()
    {
    	htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir")+"/test-output/myExtentReport.html");
    	extent = new ExtentReports();
    	extent.attachReporter(htmlReporter);
    	
    	extent.setSystemInfo("OS", "Windows 10");
        extent.setSystemInfo("Host Name", "LocalHost");
        extent.setSystemInfo("Environment", "QA");
        extent.setSystemInfo("User Name", "Arman Khandelwal");
        
        htmlReporter.config().setDocumentTitle("AutomationReport");
        htmlReporter.config().setReportName("Automation Report");
        htmlReporter.config().setTheme(Theme.DARK);
    }
    
    // ScreenShot Method
    public static String getScreenShot(WebDriver driver, String screenShotName) throws IOException
    {
    	String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
    	TakesScreenshot scrShot = (TakesScreenshot) driver;
    	File source = scrShot.getScreenshotAs(OutputType.FILE);
    	String scrShotFilePath = System.getProperty("user.dir") + "/Screenshots/" + screenShotName + dateName + ".png";
    	File destination = new File(scrShotFilePath);
    	FileUtils.copyFile(source, destination);
		return scrShotFilePath;
    }
    
    @AfterMethod
    public void getResult(ITestResult result) throws IOException
    {
    	if(result.getStatus() == ITestResult.FAILURE)
    	{
    		//test.log(Status.FAIL, "Test Case Failed is "+result.getName());
    		test.log(Status.FAIL, MarkupHelper.createLabel(result.getName()+" Test Case Failed due to below issues", ExtentColor.RED));
    		test.log(Status.FAIL, "Test Case Failed is "+result.getThrowable());
    		String screenShotPath = getScreenShot(driver,result.getName());
    		test.addScreenCaptureFromPath(screenShotPath);
    	}
    	
    	else if(result.getStatus() == ITestResult.SUCCESS)
    	{
    		//test.log(Status.PASS, "Test Case Passed is "+result.getName());
    		test.log(Status.PASS, MarkupHelper.createLabel(result.getName()+" Test Case Passed", ExtentColor.GREEN));
    		String screenShotPath = getScreenShot(driver,result.getName());
    		test.addScreenCaptureFromPath(screenShotPath);
    	}
    	
    	else 
    	{
    		//test.log(Status.SKIP, "Test Case Skipped is "+result.getName());
    		test.log(Status.SKIP, MarkupHelper.createLabel(result.getName()+" Test Case Skipped", ExtentColor.ORANGE));
    		test.log(Status.SKIP, "Test Case Skipped is "+result.getThrowable());
    		String screenShotPath = getScreenShot(driver,result.getName());
    		test.addScreenCaptureFromPath(screenShotPath);
    	}
    }
    
    @AfterSuite
    public void endReport() throws IOException
    {
    	extent.flush();
    	
    	if(UI_FLAG)
    	{
    		File htmlFile = new File(HTML_FILE);
    		Desktop desktop = Desktop.isDesktopSupported()?Desktop.getDesktop():null;
    		if(desktop!=null && desktop.isSupported(Desktop.Action.BROWSE)) desktop.browse(htmlFile.toURI());
    	}
    }

}
