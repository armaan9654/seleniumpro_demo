package testcases;

import static org.testng.Assert.assertEquals;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageobjects.AngularWebsite;
import pageobjects.CalculatorWebsite;
import reportgeneration.ExtentBaseTest;

public class AngCal_Test extends ExtentBaseTest
{
	public CalculatorWebsite calc;
	public AngularWebsite ang;
	public long pageLoadTime_Seconds;
	
	@BeforeTest
	public void getDriver()
	{
		System.setProperty("webdriver.chrome.driver","./chromeDriver/chromedriver.exe");
		ChromeOptions options = new ChromeOptions(); 
		options.addArguments("disable-infobars"); 
		driver = new ChromeDriver(options);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
	
	@Test(priority=2)
	public void angluarWebsiteTest()
	{
		test = extent.createTest("angluarWebsiteTest");
		ang = new AngularWebsite(driver);
		driver.get("https://angularjs.org/");
		ang.searchText("Angular");
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	}
	
	@Test(priority=1, dataProvider="getCalValues")
	public void calculatorWebsiteTest(String number1, String number2, String expectedResult)
	{
		test = extent.createTest("calculatorWebsiteTest");
		calc = new CalculatorWebsite(driver);
		driver.get("http://juliemr.github.io/protractor-demo/");
		calc.calcValue(number1, number2);
		String actual_result = calc.getResult.getText();
		System.out.println("The Actual Value is :"+actual_result);
		assertEquals(actual_result, expectedResult);
	}
	
	@DataProvider
	public Object[][] getCalValues()
	{
		return new Object[][]
				{
					{"3","5","8"},
					{"2","7","10"},
					{"5","4","9"},
					{"23","34","10"},
					{"3","5","8"},
					{"2","7","10"},
					{"3","5","8"},
					{"2","7","10"},
					{"3","5","8"},
					{"-3","7","4"},
				};
	}
}
